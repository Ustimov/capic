//
//  UACImageTableViewController.m
//  capic
//
//  Created by Artem Ustimov on 8/4/16.
//  Copyright © 2016 Artem Ustimov. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import "UACImageTableViewController.h"
#import "UACItemStore.h"
#import "UACItem.h"
#import "UACDetailViewController.h"

@interface UACImageTableViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

@implementation UACImageTableViewController

- (instancetype)init {
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        UINavigationItem * navigationItem = self.navigationItem;
        navigationItem.title = @"List";
        UIBarButtonItem * addBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(takePicture:)];
        navigationItem.rightBarButtonItem = addBarButtonItem;
        navigationItem.leftBarButtonItem = self.editButtonItem;
    }
    return self;
}

- (IBAction)takePicture:(id)sender {
    if ([[[UACItemStore sharedStore] allItems] count] > 4) {
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Max count of pictures is 5" message:@"You must delete some of alredy added images to upload a new one." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    UIImagePickerController * imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Image source" message:@"Please select image source" preferredStyle:UIAlertControllerStyleActionSheet];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {}]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    NSURL * refUrl = [info valueForKey:UIImagePickerControllerReferenceURL];
    __block NSString * name;
    __block UIImage * image = info[UIImagePickerControllerOriginalImage];
    NSData * imageData = UIImageJPEGRepresentation(image, 1.0);
    NSInteger imageSize = [imageData length] / (1024 * 1024);
    if (imageSize > 10) {
        NSString * errorMessage = [NSString stringWithFormat:@"Max image size is 10 Mb. Selected image size is %ld Mb", (long)imageSize];
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Image size error" message:errorMessage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    ALAssetsLibraryAssetForURLResultBlock resultBlock = ^(ALAsset * imageAsset) {
        ALAssetRepresentation * imageRepresentation = [imageAsset defaultRepresentation];
        name = [[imageRepresentation filename] stringByDeletingPathExtension];
        [self uploadImage:image withName:name];
    };
    ALAssetsLibrary * assetsLibrary = [[ALAssetsLibrary alloc] init];
    [assetsLibrary assetForURL:refUrl resultBlock:resultBlock failureBlock:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)addImageToTable:(UIImage*)image withName:(NSString *)name {
    UACItem * item = [[UACItemStore sharedStore] createItemWithText:@"" andImage:image];
    NSInteger lastRow = [[[UACItemStore sharedStore] allItems] indexOfObject:item];
    item.text = name;
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:lastRow inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
}

- (void)uploadImage:(UIImage *)image withName: (NSString *)name {
    UIActivityIndicatorView * activityIndicatorView = [self showActivityIndicatorView];
    NSString *const BoundaryConstant = @"----------R3ymHFg47ehdqgZCaKO6jy";
    NSString *const FileParamConstant = @"file";
    NSURL * requestURL = [NSURL URLWithString:@"http://127.0.0.1:8000/is/upload/"];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant] forHTTPHeaderField: @"Content-Type"];
    NSMutableData * body = [NSMutableData data];
    NSData * imageData = UIImageJPEGRepresentation(image, 1.0);
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@.jpg\"\r\n", FileParamConstant, name] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    NSString * postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setURL:requestURL];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * response, NSData * data, NSError * error) {
        [activityIndicatorView removeFromSuperview];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        if (!data) {
            [self showConnectionAlert];
            return;
        }
        NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        BOOL result = [json[@"result"] boolValue];
        if (result) {
            [self addImageToTable:image withName:name];
        } else {
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Max count of pictures is 5" message:@"You must delete some of alredy added images to upload a new one." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://127.0.0.1:8000/is/list/"]];
    __block NSArray * json;
    UIActivityIndicatorView * activityIndicatorView = [self showActivityIndicatorView];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * response, NSData * data, NSError * error) {
        if (!data) {
            [self showConnectionAlert];
            [activityIndicatorView removeFromSuperview];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            return;
        }
        json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        for (id object in json) {
            NSString * text = object[@"text"];
            NSString * image = object[@"image"];
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"http://127.0.0.1:8000", image]];
            NSData * data = [NSData dataWithContentsOfURL:url];
            UIImage * img = [[UIImage alloc] initWithData:data];
            [self addImageToTable:img withName:text];
        }
        [activityIndicatorView removeFromSuperview];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }];
}

- (void)showConnectionAlert {
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Connection error" message:@"There is no internet connection or server unavailable." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[UACItemStore sharedStore] allItems] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    NSArray * items = [[UACItemStore sharedStore] allItems];
    UACItem * item = items[indexPath.row];
    cell.textLabel.text = [item text];
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIActivityIndicatorView * activityIndicatorView = [self showActivityIndicatorView];
        NSArray * items = [[UACItemStore sharedStore] allItems];
        __block UACItem * item = items[indexPath.row];
        NSString * post = [NSString stringWithFormat:@"name=%@", item.text];
        NSData * postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSString * postLength = [NSString stringWithFormat:@"%d", [postData length]];
        NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"http://127.0.0.1:8000/is/delete/"]];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * response, NSData * data, NSError * error) {
            if (!data) {
                [self showConnectionAlert];
                [activityIndicatorView removeFromSuperview];
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                return;
            }
            [[UACItemStore sharedStore] removeItem:item];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [activityIndicatorView removeFromSuperview];
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        }];
    }
}

- (UIActivityIndicatorView *)showActivityIndicatorView {
    __block UIActivityIndicatorView * activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(135, 140, 50, 50)];
    activityIndicatorView.color = [UIColor lightGrayColor];
    [activityIndicatorView startAnimating];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self.view addSubview:activityIndicatorView];
    return activityIndicatorView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UACDetailViewController * detailViewController = [[UACDetailViewController alloc] init];
    NSArray * items = [[UACItemStore sharedStore] allItems];
    UACItem * item = items[indexPath.row];
    detailViewController.item = item;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

@end
