//
//  UACItemStore.m
//  capic
//
//  Created by Artem Ustimov on 8/4/16.
//  Copyright © 2016 Artem Ustimov. All rights reserved.
//

#import "UACItemStore.h"
#import "UACItem.h"

@interface UACItemStore()

@property (nonatomic) NSMutableArray * privateItems;

@end

@implementation UACItemStore

+ (instancetype)sharedStore {
    static UACItemStore * sharedStore = nil;
    if (!sharedStore) {
        sharedStore = [[self alloc] initPrivate];
    }
    return sharedStore;
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Singleton" reason:@"Use +[UACItemStore sharedStore]" userInfo:nil];
    return nil;
}

- (instancetype)initPrivate {
    self = [super init];
    if (self) {
        _privateItems = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSArray *)allItems {
    return self.privateItems;
}

- (UACItem *)createItemWithText:(NSString *)text andImage:(UIImage *)image {
    UACItem * item = [[UACItem alloc] init];
    item.text = text;
    item.image = image;
    [self.privateItems addObject:item];
    return item;
}

- (void)removeItem:(UACItem *)item {
    [self.privateItems removeObjectIdenticalTo:item];
}

@end
