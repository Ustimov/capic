//
//  UACDetailViewController.m
//  capic
//
//  Created by Artem Ustimov on 8/4/16.
//  Copyright © 2016 Artem Ustimov. All rights reserved.
//

#import "UACDetailViewController.h"
#import "UACItem.h"
#import "KeepLayout.h"

@interface UACDetailViewController ()

@end

@implementation UACDetailViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        UINavigationItem * navigationItem = self.navigationItem;
        navigationItem.title = @"Detail";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    CGFloat imageWidth = self.item.image.size.width;
    CGFloat imageHeight = self.item.image.size.height;
    CGFloat ratio;
    if (screenWidth < screenHeight) {
        ratio = screenWidth / imageWidth;
    } else {
        ratio = screenHeight / imageHeight;
    }
    UIImageView * imageView = [[UIImageView alloc] initWithImage:self.item.image];
    [self.view addSubview:imageView];
    [imageView keepSize:CGSizeMake(imageWidth * ratio - 2 * 10 * ratio, imageHeight * ratio - 10 * ratio)];
    [imageView keepCentered];
}

@end
