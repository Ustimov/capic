//
//  UACItem.h
//  capic
//
//  Created by Artem Ustimov on 8/4/16.
//  Copyright © 2016 Artem Ustimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UACItem : NSObject

@property (nonatomic, strong) NSString * text;

@property (nonatomic, strong) UIImage * image;

@end
