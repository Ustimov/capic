//
//  UACDetailViewController.h
//  capic
//
//  Created by Artem Ustimov on 8/4/16.
//  Copyright © 2016 Artem Ustimov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UACItem;

@interface UACDetailViewController : UIViewController

@property (nonatomic, strong) UACItem * item;

@end
