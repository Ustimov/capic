//
//  AppDelegate.m
//  capic
//
//  Created by Artem Ustimov on 8/4/16.
//  Copyright © 2016 Artem Ustimov. All rights reserved.
//

#import "UACAppDelegate.h"
#import "UACImageTableViewController.h"

@interface UACAppDelegate ()

@end

@implementation UACAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UACImageTableViewController * imageTableViewController = [[UACImageTableViewController alloc] init];
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:imageTableViewController];
    self.window.rootViewController = navigationController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

@end
