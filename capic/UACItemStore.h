//
//  UACItemStore.h
//  capic
//
//  Created by Artem Ustimov on 8/4/16.
//  Copyright © 2016 Artem Ustimov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class UACItem;

@interface UACItemStore : NSObject

@property (nonatomic, readonly) NSArray * allItems;

+ (instancetype)sharedStore;
- (UACItem *)createItemWithText:(NSString *)text andImage:(UIImage *)image;
- (void)removeItem:(UACItem *)item;

@end
